<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title> Авторизация </title> <!-- стили желательно, не здесь но можно и здесь -->
	<style>	
		.text {						/* центрирование текста применительно к части "text */
			text-align: center; 
		}
		.auth_form {
			width: 300px;
			margin: auto;
		}
		
	</style> 
</head>
<body>					
<?php
if(!empty($error))
	echo $error; // принято include error, error.php и в него вёрстку поместить. требуется к нему разработать стиль 
  ?>			
	<div class="text">
		<big>Добро пожаловать!</big><br>
		<big>Пожалуйста авторизируйтесь</big>	
	</div>

	<div class="auth_form">			<!-- form>(p>input)*3 - испльзуется для создания скозу нескольких строк ( у меня почему-то не работал потом надо разобраться ) -->			<!--div используем для того что бы разместить это участок по центру -->
		<form method="post">
				<!-- method post используется для того что бы данные не были видны ( поскольку там пароль)-->
			<p> Логин: <input name="login" type="text"></p>			<!--required говорит о том что поле обязательное для заполнения (поменял на всплывающие ошибки)-->
			<p> Пароль: <input name="password" type="password"></p>
			<p><input name="authorization" type="submit" ></p>
												<!--данные после ввода пользователем отправляются снована эту же страницу, то есть страница должна отработать полученные данные после чего что-то ответить пользователю-->
	</div>
</body>
</html>